# ROUND ROBIN DNS TEST

- ### 1! crea una red
```bash
docker network create net
```

- ### 2! crea dos contenedors con el mismo --net-alias
```bash
# alias search
docker container run -d --net nombre_net --net-alias search elasticsearch:2
docker container run -d --net nombre_net --network-alias search elasticsearch:2
```

- ### 3! lista los nuevos contenedores y fijate en el puerto que usan pa comunicarse
```bash
docker container ls
```
en el output deberías ver que usan el puero 9200 o algo así...

- ### 4! Haga un nslookup
```bash
docker container run --rm --net nombre_net alpine nslookup search
# --rm es para borrarlo cuando se cierre el daemon o se exitee del container
# o en este caso, despues de hacer `nslookup`
```
en el output se ve que se listan los 2 elasticsearch creados con sus --net-alias e ip

- ### 5! Haga un curl para llegar a search
```bash
docker container run --rm --net-alias nombre_net centos curl -s search:9200

docker container run --rm --net-alias nombre_net centos curl -s search:9200

docker container run --rm --net-alias nombre_net centos curl -s search:9200

docker container run --rm --net-alias nombre_net centos curl -s search:9200
```
en el output se ve como llega indistintamente a cualquiera de los 2 search.

- ### 6! Limpia todo
```bash
docker container rm ...wenoyatedije...
```