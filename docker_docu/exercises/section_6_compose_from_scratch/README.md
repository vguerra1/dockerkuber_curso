# LEVANTAR UN MULTICONTAINER SERVICE

## 1! hacete un .yml que levante drupal y postres
```yaml
version:2

services:
    drupal:
        image: drupal
        ports:
            - "8080:80"
            # busca bien los puertos para comunicarse en la docu de drupal
        volumes:
            - drupal-modules:/var/www/html/modules
            - drupal-profiles:/var/www/html/profiles
            - drupal-sites:/var/www/html/sites
            - drupal-themes:/var/www/html/themes
    postgres:
        image: postgres
        environment:
            - POSTGRES_PASSWORD=mypasswd

volumes:
    drupal-modules:
    drupal-profiles:
    drupal-sites:
    drupal-themes:
```

## 2! levantalo
```bash
docker-compose up
```

## 3! matalo
```bash
docker-compose up
```