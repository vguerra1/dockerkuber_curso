# DATABASE UPGRADE WITH NAMED VOLUMES

## 1! Primero crea un PostgreSQL de la versión 9.6.1
```bash
docker container run -d -e POSTGRES_PASSWORD=mypasswd --name psql -v psql:/var/lib/postgresql/data postgres:9.6.1
```
*(POSTGRES_PASSWORD=mypasswd es pa ponerle la clave)*

la parte `-v psql:/var/lib/postgresql/data` es para tener un volumen que persista, 
y este esté montado en la carpeta del contenedor que contiene las bases de datos.
*(puede verse en el Dockerfile de la imagen (o en hub.docker.com) como se configura*
*por defecto que la base de datos este en `/var/lib/postgresql/data`)*

## 2! miralo mientras se ejecuta
```bash
docker container logs -f psql
```
*-f es como follow pa seguir o keseyo*.
Aquí se pueden ver todas las configuraciones que hacer Postgres

## 3! Oh no! Hay que actualizar a postgres:9.6.2!
bueno para el contenedor
```bash
docker container stop psql
```

## 4! Crea otro contenedor pero montale el mismo volumen que tenía el `--name psql`
```bash
docker container run -d -e POSTGRES_HOST_AUTH_METHOD=trust --name psql2 -v psql:/var/lib/postgresql/data postgres:9.6.2
```
*(POSTGRES_HOST_AUTH_METHOD=trust pa que deje pasar cualquier conexión)*

## 5! mira el log completo de este nuevo contenedor
```bash
docker container logs psql2
```
Fijate que no hizo todas la confiruaciones y todo denuevo (esa información estaba guardada en el volumen `-v psql`)
