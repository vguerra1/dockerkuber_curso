# DOCKER-COMPOSE

## 1! Docker Compose configuration file `docker-compose.yml` file
Es un archivo que lleva las configuracion del servicio que van a proveer los contenedores.
```yaml
version: 3.1 # version

service: # container. same as docker run
    servicename_1: # name of the service (is also a DNS name in the network)
        image: # opcional si usas build
        command: # opcional si se remplaza el `CMD` de la imagen
        environment: # opcional, igual a `docker run ... -e ...`
        volumes: # opcional, igual que en `docker run ... -v ...`
            # si son varios se listan con -
            - ./mas/:./o
            - ./menos/:./asi
        ports: # optional, igual que en `docker run ... -p ...`
        depends_on: # nombre de servicio
            - o_servicios
            - que_necesita
            - para partir
            # le dira a compose que inicie esos primero
        #...
        #...
        #... won son caleta de opciones
    servicename_2: # name of another service

volumes: # opcional, igual a `docker volume create ...`

networks: # opcional, igual a `docker network create ...`
```

## 2! COMPOSING

- ### 2.1! descarga docker compose primero 
(busca que version poner en [VERSION] en https://github.com/docker/compose/releases)
```bash
sudo curl -L https://github.com/docker/compose/releases/download/[VERSION]/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
# ya listo estamo'
```
- ### 2.2! composea y levanta
Levanta los servicios según la configuración del archivo `./docker-compose.yml`.
```bash
docker-compose up
# para usar otro archivo .yml
docker-compose -f /ruta/del.yml up
# con `-d` para detachearlo
```

- ### 2.3! matalo
```bash
docker-compose down
# para asesinar los volumenes tambien
docker-compose down -v 
```

- ### 2.4! otras funcionalidades
En general parecido que el CLI de `docker` (vea el `docker-compose --help`)

## 3! Agregar building
para buildear la imagen...
```yaml
version: '2'

services:
    proxy:
        build:
            context: . # desde donde va a buildear
            dockerfile: nginx.Dockerfile # Dockerfile pa buildear
        image: nginx-custom # nombre de la imagen buildeada
        ports:
            - '80:80'
    web:
        image: httpd
        volumes:
            - ./html:/usr/local/apache2/htdocs
```

y weno el `nginx.Dockerfile` habría sido así

```Dockerfile
FROM nginx:1.11

COPY nginx.conf /etc/nginx/conf.d/default.conf
```
*NOTA: si la imagen `nginx-custom` existe, no volverá a buildear (así que remueve esa linea del `.yml` remueve el campo `image`  para que rebuildee)*




