# VOLUMES

## 1! Data persistente - Volumenes
A un contenedor se le puede asignar una parte del sistema de archivos del host.

Es lo que pasa cuando se crea un contenedor de un servicio de base de datos por ejemplo `mysql`. mira. 
```bash
# ok crea un container mysql
docker container run -d --name db -e MYSQL_RANDOM_ROOT_PASSWORD=yes mysql
# inspeccionalo y fijate en el campo 
# - Mounts.Type = "volume"
# y
# - Mounts.Name = "UN_ID_DEL_VOLUMEN"

# ok ahora asesina el contenedor
docker container rm -f mysql

# ahora lista los volumenes...
docker volume ls
# en el output deberías ver el sistema de archivos de la base de datos que
# esta en tu sistema de archivos local, PERO SE MONTA EN EL CONTENEDOR.
```

- ### 1.1! asignar manualmente un volumen
Se hace con la opción al momento de crear el container `-v nombre_volumen:/ruta/local/del/host/para/montar/en/contenedor`.
Con esta opción se puede montar `/ruta/local/del/host/para/montar/en/contenedor` en el contenedor y ubicarlo por el tag `nombre_volumen`.
```bash
# ejemplo...
docker container run -d --name mysql -e MYSQL_ALLOW_EMPTY_PASSWORD=True -v mysql-db:/local/dir/4/container/mysql mysql
```

- ### 1.2 borrar un volumen
```bash
docker volume rm "id_completo_del_volumen"
# o
docker volume rm "el_nombre_o_tag_que_le_pusiste"
```

- ### 1.3 borrar los volumenes sin usar
```bash
docker volume prune
# te va a preguntar si estay seguro :)
```

- ### 1.4 crear un volumen antes de montarlo en el contenedor (si quiere...)
```bash
docker volume create "nombre_volumen"
# bueno tiene mas opciones pero... pero.
```

## 2! Bind Mounting (data persistente en el host)
Esta funcionalidad te deja montar un directorio (o archivo) del host, directo al container.,
Tonces, ahora el container cuando haga cambios sobre este directorio, estos persistirán tras la detención de este.

- ### 2.1 Montado de directorio
Se monta una ubicación con la opción `-v`, pero en vez de usar un tag o alias para el volumen, se usa un directorio del host (debe ser una ruta completa empezada en `/`).
```bash
docker container run -d --name nombre_container -v /ruta/local/a/mapear:/destino/en/el/contenedor/
```
**PROTIP: se puede usar `$(pwd)` para poner el directorio de trabajo actual en vez de `/ruta/local/a/mapear`**

- ### 3! EJERCICIO es en .../docker_docu/exercises