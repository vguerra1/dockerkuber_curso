# MIS APUNTES :)

## Instala docker

- ### En Ubuntu
```bash
sudo snap install docker
# o
sudo apt-get install docker.io
```

## - 0! Levanta un NUEVO container
```bash
docker container run "imagen"
```
esto creará un nuevo container con la imagen dicha

## 1! Levanta tu primer container (con nginx)
```bash
docker container run --publish 80:80 nginx
```
^ ese comando descargo la ultima imagen de nginx, inició un container con la imagen, 
y abrió el trafico en la maquina local por el puerto 80 (si falla, mandale
`sudo chmod 666 /var/run/docker.sock`).

Si abres localhost en tu navegador, se verá el nginx

- ### 1.1! detachealo
para que que corra en el background...
```bash
docker container run --publish 80:80 --detach nginx
```
Esto retorna un ID
- ### 1.2.1! lista los containers (que estan corriendo)
```bash
docker container ls
```
Ahi salen los id
- ### 1.2.2! lista los containers (todos)
```bash
docker container ls -a
```

- ### 1.3.1! Para un container
para que pare
```bash
docker container stop "el_id"
```

- ### 1.3.2! Crearlo ahora con nombre clever
```bash
docker container run --publish 80:80 --detach --name "nombre_clever" nginx
# o
docker container run --publish 80:80 -d --name "nombre_clever" nginx
```
- ### 1.3.3! Inicia un container
para que pare
```bash
docker container start "nombre_clever"
```


- ### 1.4! mira los logs de un docker
```bash
docker container logs "nombre_clever"
# o
docker container logs "el_id"
```

- ### 1.5! A ver mira los procesos un container
```bash
docker container top "nombre_clever"
# o
docker container top "el_id"
```

- ### 1.6.1! elimina container (o containeres)
```bash
docker container rm "nombre_clever_1" "nombre_clever_2" "nombre_clever_3" 
```

- ### 1.6.2! si hay algun container corriendo no te va a dejar... tonces...
```bash
docker container rm -f "nombre_clever_1" "nombre_clever_2" "nombre_clever_3" 
```

- ### 1.6.3! corre el container con environment variables
```bash
docker container run --env variable_1='variable' imagen
#o
docker container run -e variable_1='variable' imagen
```

- ### 1.6.4! inspecciona un container
```bash
docker container inspect "nombre_clever"
```
trae un .json con toda la metadata sobre como se crea el container

- ### 1.6.5! mira las stats (RAM, CPU, PID, CONTAINER, ID, NET I/O, BLOCK I/O) de todos los containers
```bash
docker container stats --all
```
#### NOTA: sobre los puertos port1:port2 en el `docker container run ...`
port1 es el puerto local y port2 es el puerto del container

## 2! Levanta varios container en varios puertos con variables de entorno y todo

- ### 2.1! levanta un mysql que genere pwd aleatoria
```bash
docker container run -d -p 3306:3306 --name db -e MYSQL_RANDOM_ROOT_PASSWORD=yes mysql
```

- ### 2.2! mira la pwd que generó
```bash
docker container logs db
```
*por ahí debe salir...*

- ### 2.3! levanta el apache webserver
```bash
docker container run -d --name webserver -p 8080:80 httpd
```

- ### 2.4! ahora un nginx
```bash
docker container run -d --name proxy -p 80:80 nginx
```

- ### 2.5! matalos a todos
```bash
docker container rm proxy webserver db
```

## 3! Shell adentro del container. 

- ### 3.1! para uno nuevo
```bash
docker container run -it --name "nombre_clever" nginx bash
```
`-it` son las opciones combinadas `-t` (simularle una terminal real, a lo ssh) `-i` (mantenerlo como shell interactivo)

- ### 3.2! para uno ya existente
```bash
docker container exec -it "nombre_clever" bash
# ^ ese es pa ejectuarlo con un comando en particular...
```
Así le podis descargar cosas o que se yo

- ### 3.3! OYE PERO UN UBUNTU PO
se crea un ubuntu
```bash
docker container run -it --name ubuntu ubuntu
```
nota que parte con un bash porque... won... es ubuntu

- ### 3.4! inicia el shell de tu nuevo ubuntu
```bash
docker container start -ai ubuntu
```

- ### 3.5! Instalale cosas
```bash
root@ID:/# apt-get update
root@ID:/# apt-get install algun_paquete
```

## 4! NETWORKEANDO CON CONTAINERS

- ### 4.1! volviendo... levanta un container con nginx en nuestro puerto 80
```bash
docker container run -p 80:80 --name webhost -d nginx
```

- ### 4.2! para ver que puertos trafican datos del host al container
```bash
docker container port webhost
# el formato del output es rebonito mira:
# 80/tcp -> 0.0.0.0:80
```

- ### 4.3! para obtener la ip del container
```bash
docker container inspect --format "{{.NetworkSettings.IPAddress }}" webhost
```
el `--format` es para sacarle los campos especificos que necesitas del json
gigante que te escupe `docker container inspect ...`

- ### 4.4! listar redes
```bash
docker network ls
```
debería salir algo como...
```bash
NETWORK ID          NAME                DRIVER              SCOPE
89291c369322        bridge              bridge              local
ab73073ebe3b        host                host                local
80c87991db59        none                null                local
```
bueno ahí `bridge` (podría tener otro nombre como `docker0`). 
`bridge` es la red virtual que se conecta a nuestra red local (o la que le digai)

- ### 4.5! inspecciona la red bridge mira...
```bash
docker network insect bridge
```
escupirá un json gigante.

puedes ver los containers que tiene conectados bajo el campo "Containers".
```bash
docker network inspect --format "{{.Containers}}" bridge
```
y las ip que tiene asignadas bajo el campo `IPAM.Config`

- ### 4.6! crea una nueva red
```bash
docker network create my_app_net
```

- ### 4.6.1! creala con otro driver para manejar la red. 
Por defecto usa el driver `bridge`, por defecto también puede aceptar `overlay`. 
*Igual se pueden usar third-party drivers...*

```bash
docker nerwork create --driver el_driver my_app_net
```


- ### 4.7! crea un nuevo container nginx y conectalo a esa red
```bash
docker container run -d --name "nombre_clever_2" --network my_app_net nginx
```

- ### 4.8! conectar un container existente a otra red
```bash
docker network connect otra_red container_existente
```
ahora puedes ver con `docker container inspect container_existente` que esta conectado a su red 
original (si tenía) y conectado a `otra_red` también (bajo el campo "Networks")

- ### 4.9! desconectarlo...
```bash
docker network disconnect otra_red container_existente
```

## 5! DNS ing en docker

Los containers usan sus mismos nombres con nombre del host (DNS).

Por ejemplo, si existen dos contenedores conectados a una misma red (NO LA POR DEFECTO `bridge`) y se pingean entre ellos se pueden encontrar por el nombre del contenedor. mira.

```bash
# CREA LOS CONTENEDORES
docker container run -d -it --name nombre_clever_1 ubuntu
docker container run -d -it --name nombre_clever_2 ubuntu

# LOS PREPARA PARA PINGEAR
docker container exec -it nombre_clever_1 apt-get update
docker container exec -it nombre_clever_1 apt-get install iputils-ping

docker container exec -it nombre_clever_2 apt-get update
docker container exec -it nombre_clever_2 apt-get install iputils-ping

# CREA LA RED Y CONECTA LOS CONTENEDORES A ESA RED
docker network create clever_net
docker network connect clever_net nombre_clever_1
docker network connect clever_net nombre_clever_2

# PING !
docker container exec -it nombre_clever_1 ping nombre_clever_2
# o
docker container exec -it nombre_clever_2 ping nombre_clever_1
# el resultado será glorioso
```
*NOTA: LA RED `bridge` NO TRAE ESTA RESOLUCION AUTOMATICA DE DNS*