# IMAGES

## 0! image
esta hecha de varias capas de operaciones de:
```bash
- cambios en el sistema de archivos.
- cambios de variables y metadata.
```

- ### 0.1! lista las imagenes
```
docker image ls
```

## 1! layers
las layers son capas de operaciones secuenciales o dependencias que se hacen en el contenedor al iniciarse.

Por ej: si quisiera una imagen de `python`, tendría sus layers así.
```bash
- layer0: instalar ubuntu.
- layer1: instalar apt en ubuntu.
- layer2: instalar python en el ubuntu con `apt-get install python` o algo así.
- layer3: ejecutar python.
```
- ### 1.1! puedes ver las layers con el comando `history`
```bash
docker image history nombre_de_imagen
```
verás en el output todas las capas, y en la primera capa (la más reciente), se ve el id de la imagen.

- ### 1.2! inspect image
```bash
docker image inspect nombre_de_imagen
```
aquí sale toda la metadata de la imagen, arquitectura, que ejecuta al inicio, con que variables de entorno, 
su configuracion, los puertos que usa...

## 2! TAGS
Las imagenes tiene sus id (imposibles) y tags, los tags son identificadores de la imagen.

Los tags llevan 3 identificadores:
```bash
- RESPOSITORY (2 identificadores): 
    nombre_user/nombre_repo (si no es oficial del equipo dockers)
    o
    /nombre_repo (si es oficial del equipo dockers)
- TAG (el otro):
    relativo a la versión de la imagen.
    un puntero que apunta a un commit especifico de la imagen.
```

- ### 2.1 ponerle un tag a la imagen
```bash
docker image tag nombre_imagen nombre_usuario/nombre_tag
```
si haces `docker image ls` ahora te aparecerá otra imagen con el tag `nombre_usuario/nombre_tag`.

- ### 2.2 pushear imagen a dockerhub
```bash
# OJO QUE ESTÁ PUBLICO SI NO SE ESPECIFICA EN LA GUI DE hub.docker.com !
docker image push nombre_usuario/nombre_tag
```

- ### 2.? Hay que loguearse primero
```bash
# primero crea una cuenta en hub.docker.com
docker login
# te logueas :)
```
esto guardará la informacion de logueo en la maquina local, para que no se guarde...
```bash
docker logout
```
ahora se borró la informacion de logueo

- ### 2.3 prueba poniendole "versión" al tag
```bash
docker image tag nombre_usuario/nombre_tag nombre_usuario/nombre_tag:nombre_version
```
AHORA PUEDES SUBIR OTRA VERSION 

- ### 2.4 PARA QUE SEA PRIVADO SE DEBE ESPECIFICAR AL MOMENTO DE CREARLO EN LA GUI DE hub.docker.com

## 3! Dockerfile
Es una receta para armar una imagen de un contenedor... lleva hartas cosas... mira.

- 3.1! la estructura del Dockerfile
```Dockerfile
# btw.. cada uno de estos "THE" forman una capa de la imagen (así que el orden importa)

# THE `FROM`
FROM debian:jessie
# usualmente una versión MINIMA de sistema operativo de linux
# si se quiere partir el container "vacío" usar `FROM scratch`

# THE `ENV`
ENV VARIABLE_DE_ENTORNO valor_variable_de_entorno
# son variables de entorno

# THE `RUN`
RUN apt-get install nombre_paquete

RUN apt-get install otro_nombre_paquete

RUN wget google.com

RUN cualquier_wea
# es uno o varios comandos que se ejecutarán en la imagen para hacer camb.ios en esta
# como instalar paquetes, correr algún script, descargar algo... keseyo

RUN apt-get install nombre_paquete \
    && apt-get install otro_nombre_paquete \
    && wget google.com \
    && cualquier_wea
    && rm -rf /var/lib/apt/lists/*
    # ^ el `rm -rf /var/lib/apt/lists/*` 
    # pa borrar el CACHE de apt (pesaa la wea) 
    # ----------------------------------------
    && git clone --branch nombre_branch --single-branch --depth 1 https://git...
    # ^ Si descargar algo vía git clone, usar las opciones
    # `... --branch nombre_branch --single-branch --depth 1 ...`
    # pa que descargue menos weas
    # NO NECESITAS TODO EL REPOSITORIO, SOLO UN COMMIT
    # ----------------------------------------
    # FIJATE SI HAY QUE CAMBIAR algo con `chown`
    && chown -R [user]:[group] [directory/in/container (?)]
    #
    
# se puede hacer un comando muy largo tambien, para que todo el conjunto de operaciones
# quede en una sola layer.

RUN cosas_relativas_a_logging_propias_de_la_apliacion
# aunque docker gestiona el logueo por ti...

EXPOSE port1 
# o...
EXPOSE port2 port3 port4
# expone estos puertos en la red virtual en docker
# ^^^ ESTO NO PULICA LOS PUERTOS AL HOST
# vvv ESTO PUBLICA LOS PUERTOS AL HOST
# -p o -P se usa para abrir los puertos a otro host

CMD ["el_ultimo_comando","-con","sus_parametros;"]
# dice que comando correr cuando se lance el container

# ejemplo
CMD ["/bin/bash"]
# ^^^ REQUERIDO
```
Varias imagenes oficiales ya llevan todo esto hecho, lo que haremos será agregarle más layers a estas imagenes. por ejemplo otros **RUN** despues de buildear todo lo de la imagen.

- ### 3.2! build the image
```bash
docker image build -t nombre_super_clever .
# el puntito al final es pa decir que ejecute el Dockerfile en la ruta "./"
```
Si hacer un cambio en el Dockerfile y ejecutas `docker image build -t nombre_super_clever .` 
se actualizará la versión `latest` de esa imagen (**solo se re ejecutaran las cosas del Dockerfile que vayan despues (más abajo en el codigo) del cambio que se hizo... FIJATE EN EL ORDEN MACHUCAO TE DISEN**)

## 4! extendiendo imagenes oficiales
Digamos que se quiere cambiar un archivo de las dependencias de una imagen oficial. bueno sería así para nginx
- ### 4.1! cambio en Dockerfile
```Dockerfile
FROM nginx:latest
# nuestra imagen oficial base

WORKDIR /usr/share/nginx/html
# cambiar el working directory así en vez de `RUN cd /some/path`

COPY local/machine/folder/index.html index.html
# ^ this one copied local/machine/folder/index.html from my machine
# to the docker container /usr/share/nginx/html directory.

# ¿OYE PERO NO TERMINA CON CMD?
# Nah, todo eso lo trae el `FROM nginx:latest`
```

- ### 4.2! rebuild
buildeas denuevo...
```bash
docker image build -t nombre_super_clever .
# o se puede especificar otro archivo especifico con `-f` o `--file`

```

- ### 4.3! repushear a DockerHub
Primero tendrías que renombrar el tag a algo como nombre_usuario_dockerhub/nombre_super_clever y luego pushearlo
```
docker image tag nombre_super_clever:latest nombre_usuario_dockerhub/nombre_super_clever:latest
docker image push nombre_usuario/nombre_super_clever:latest
```

## 5! revisa `docker image prune`
noseyo... decía el loquito del curso...